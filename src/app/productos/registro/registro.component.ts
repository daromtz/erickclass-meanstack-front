import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductoServicioService } from '../producto-servicio.service';
import { Producto } from '../producto.interface';

/*+esto es un comentario para probar si funciona 
jejejeje
*/
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  formularioRegistraProducto: FormGroup;

  constructor(private fbuild: FormBuilder, private productoS:ProductoServicioService) { }

  ngOnInit(): void {
    this.crearFormulario();
  }
/*creacion de formulario chidooo
*/
  crearFormulario() {
    this.formularioRegistraProducto = this.fbuild.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      urlImg: ['', [Validators.required]],
      precio: ['', [Validators.required]],
      marca: ['', [Validators.required]],
    });
  }
/* se creo el registro jejejeje
*/
  registrar() {
    const productito: Producto = this.formularioRegistraProducto.value;
    this.productoS.registrarProducto(productito).subscribe(res =>{
      console.log(res);
    })
  }

}
